=============================
dj-sessions
=============================

.. image:: https://badge.fury.io/py/dj-sessions.png
    :target: https://badge.fury.io/py/dj-sessions

.. image:: https://travis-ci.org/pydanny/dj-sessions.png?branch=master
    :target: https://travis-ci.org/pydanny/dj-sessions

Enhanced session traciking for Django

Documentation
-------------

The full documentation is at https://dj-sessions.readthedocs.org.

Quickstart
----------

Install dj-sessions::

    pip install dj-sessions

Then use it in a project::

    import djsessions

Features
--------

* TODO

Running Tests
--------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install -r requirements_test.txt
    (myenv) $ python runtests.py

Credits
---------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage
