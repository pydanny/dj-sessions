============
Installation
============

At the command line::

    $ easy_install dj-sessions

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv dj-sessions
    $ pip install dj-sessions
